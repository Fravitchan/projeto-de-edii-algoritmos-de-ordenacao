/*Projeto ED2 - Estrutura de Dados II

Grupo: Fl�via, Isabella V., Luiz, Mateus, Nathann e Paulo
Curso: An�lise e Desenvolvimento de Sistemas - IFSP
Professor: Antonio Tartaglia
Entrega: 11/10/2019
*/

#include <stdio.h>
#include <stdlib.h>
#include "algoritmosOrdenacao.h"

int main()
{
    printf("\n\t\t========== Algoritmos de Ordenacao ==========\n");

    //declara��o de variaveis
    int escolha, opc_qtd;

    //Menu principal
    do{
        escolha = menu_principal();
        switch(escolha){
            case 1:
                printf("\n\t\t===== BubbleSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tVetor gerado: \n");
                        gera_vetor(opc_qtd);
                        break;
                    case 2:
                        printf("\n\tteste");
                        gera_vetor(opc_qtd);
                        break;
                }

                break;
            case 2:
                printf("\n\t\t===== SelectionSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 3:
                printf("\n\t\t===== InsertionSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 4:
                printf("\n\t\t===== ShellSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 5:
                printf("\n\t\t===== HeapSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 6:
                printf("\n\t\t===== MergeSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 7:
                printf("\n\t\t===== RadixSort (LSD) =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 8:
                printf("\n\t\t===== QuickSort =====");
                opc_qtd = menu_qtdElementos();
                switch(opc_qtd){
                    case 1:
                        printf("\n\tteste");
                        break;
                }

                break;
            case 9:
                printf("\n\tOpcao de teste");
                break;
            default:
                printf("\n\t-----> Escolha uma op��o valida!\n"); //chama o menu novamente.
                break;

        }
    }while(escolha != 9);

    return 0;
}
